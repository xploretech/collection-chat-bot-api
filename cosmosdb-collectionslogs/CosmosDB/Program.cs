﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Cosmos;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO.Compression;
using System.Threading;
using System.Net.Mail;
using System.Net;
using Renci.SshNet;
using System.IO;

namespace CosmosDB
{
    public class Program
    {
        
        static async Task Main(string[] args)
        {
            Task T= QueryDB();
            while(true)
            {
                if (T.IsCompleted)
                    break;
            }
       
           List<string> mailids = new List<string>(ConfigurationManager.AppSettings["Mailid"].Split(new char[] { ',' }));
           await SendFile(mailids,  "PFA");
        
            
        }


        
       

        public static async Task<bool> SendFile(List<string> toMailId,  string htmlMessage)
        {

            string zipPath = "";
            if (toMailId.Count==0)
            {
                throw new ArgumentException("no to address provided");
            }

            if (string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["Subject"]))
            {
                throw new ArgumentException("no subject provided");
            }

            string filename = "", startPath="";
            string mode = ConfigurationManager.AppSettings["Mode"];
            if (mode.Equals("production", StringComparison.OrdinalIgnoreCase))
            {
                filename = "prodconversation" + DateTime.Now.ToString("yyyy-MM-dd_HHmm") + ".zip";
                startPath = ConfigurationManager.AppSettings["ProdPath"];
            }
            else
            {
                filename = "testconversation" + DateTime.Now.ToString("yyyy-MM-dd_HHmm") + ".zip";
                startPath = ConfigurationManager.AppSettings["TestPath"];
            }


            if (!Directory.Exists(startPath))
                Directory.CreateDirectory(startPath);

            zipPath = ConfigurationManager.AppSettings["ZipPath"];

            if (!Directory.Exists(zipPath))
                Directory.CreateDirectory(zipPath);
            zipPath = ConfigurationManager.AppSettings["ZipPath"] + filename; ;
            DirectoryInfo di = new DirectoryInfo(startPath);
            if (di.GetFiles("*.txt").Length > 0)
            {
                Console.WriteLine("File Found");
                try
                {
                    if (File.Exists(zipPath))
                        File.Delete(zipPath);
                    ZipFile.CreateFromDirectory(startPath, zipPath, CompressionLevel.Fastest, true);
                    Thread.Sleep(1000);
                    Sentftpfile(zipPath);
                    await SendEmail(zipPath, htmlMessage, toMailId);
                    //using (var client = new SftpClient(ConfigurationManager.AppSettings["Host"], int.Parse(ConfigurationManager.AppSettings["FTPPort"]),
                    //ConfigurationManager.AppSettings["Username"], ConfigurationManager.AppSettings["FtpPassword"]))
                    //{
                    //    client.Connect();
                    //    if (client.IsConnected)
                    //    {
                    //        using (var fileStream = new FileStream(zipPath, FileMode.Open))
                    //        {

                    //            client.BufferSize = 8 * 1024;
                    //            client.UploadFile(fileStream, Path.GetFileName(zipPath));
                    //        }

                    //    }
                    //    else
                    //    {
                    //        return false;
                    //    }
                    //}

                    //string fromemail = ConfigurationManager.AppSettings["FromMail"];
                    //string pwd = ConfigurationManager.AppSettings["Password"];
                    //MailMessage message = new MailMessage();
                    //foreach (string s in toMailId)
                    //{
                    //    message.To.Add(s);

                    //}

                    //message.From = new MailAddress(fromemail);
                    //message.Subject = ConfigurationManager.AppSettings["Subject"];
                    //message.SubjectEncoding = System.Text.Encoding.UTF8;
                    //message.Body = htmlMessage;
                    //Attachment attachment;
                    //attachment = new System.Net.Mail.Attachment(zipPath);
                    //message.Attachments.Add(attachment);
                    //message.BodyEncoding = System.Text.Encoding.UTF8;
                    //message.IsBodyHtml = false;
                    //message.Priority = MailPriority.High;
                    //Console.WriteLine(ConfigurationManager.AppSettings["Smptp"]);
                    //SmtpClient emailClient = new SmtpClient(ConfigurationManager.AppSettings["Smptp"], int.Parse(ConfigurationManager.AppSettings["SmptpPort"]));
                    //emailClient.UseDefaultCredentials = true;

                    //emailClient.Credentials = new System.Net.NetworkCredential(fromemail, pwd);//TOM@kol123
                    //emailClient.EnableSsl = true;
                    //await emailClient.SendMailAsync(message);

                    Thread.Sleep(1000);

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return false;
                    
                }
                finally
                {
                    DirectoryInfo di1 = new DirectoryInfo(startPath);

                    foreach (FileInfo file in di1.GetFiles())
                    {
                        file.Delete();
                    }

                }
                
            }
            else
            {
             await   SendEmail("", "No File generated", toMailId);
            }
            return true;




        }


        private static async Task<bool> SendEmail(string zipPath,string htmlMessage,List<string> toMailId)
        {
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            string fromemail = ConfigurationManager.AppSettings["FromMail"];
            string pwd = ConfigurationManager.AppSettings["Password"];
            MailMessage message = new MailMessage();
            foreach (string s in toMailId)
            {
                message.To.Add(s);

            }
            
            message.From = new MailAddress(fromemail);
            message.Subject = ConfigurationManager.AppSettings["Subject"];
            message.SubjectEncoding = Encoding.UTF8;
            message.Body = htmlMessage;
            if (!string.IsNullOrEmpty(zipPath))
            {
                Attachment attachment;
                attachment = new Attachment(zipPath);
                message.Attachments.Add(attachment);
            }
            
            message.BodyEncoding = Encoding.UTF8;
            message.IsBodyHtml = false;
            message.Priority = MailPriority.High;
            
            SmtpClient emailClient = new SmtpClient(ConfigurationManager.AppSettings["Smptp"], int.Parse(ConfigurationManager.AppSettings["SmptpPort"]));
            emailClient.UseDefaultCredentials = true;
            emailClient.Credentials = new NetworkCredential(fromemail, pwd);//TOM@kol123
            emailClient.DeliveryMethod=SmtpDeliveryMethod.Network;
            emailClient.EnableSsl = true;


            

            try
            {
                await emailClient.SendMailAsync(message);
            }
            catch(Exception)
            {
               
            }
            finally

            {
                Thread.Sleep(1000);
            }
            
            return true;
        }

        private static void Sentftpfile(string zipPath)
        {
            using (var client = new SftpClient(ConfigurationManager.AppSettings["Host"], int.Parse(ConfigurationManager.AppSettings["FTPPort"]),
                   ConfigurationManager.AppSettings["Username"], ConfigurationManager.AppSettings["FtpPassword"]))
            {
                client.Connect();
                if (client.IsConnected)
                {
                    using (var fileStream = new FileStream(zipPath, FileMode.Open))
                    {

                        client.BufferSize = 8 * 1024;
                        client.UploadFile(fileStream, Path.GetFileName(zipPath));
                    }

                }
                else
                {
                    return ;
                }
            }

        }

        public static async Task QueryDB()
        {
            try
            {
                CosmosClient client = new CosmosClient("https://collections-db.documents.azure.com:443/", "IA12b5YLufhUVblx1DlIlqRFMzeA1xXpp3ahKL3ol2WBuCo6x8H2mSxZw1G6TvvSgZxU39PUDSxvo7XScQw11g==");
                Database database =  client.GetDatabase("CollectionsBot-logs");
                //Container container = await database.CreateContainerIfNotExistsAsync(
                //    "conversations",
                //    "/PID",
                //    400);
                string PathValue = "";
                Container container;
                
                DateTime dt1=new DateTime();
                string mode=  ConfigurationManager.AppSettings["Mode"];
                if (mode.Equals("production", StringComparison.OrdinalIgnoreCase))
                {
                    container = database.GetContainer("conversations");
                    PathValue = ConfigurationManager.AppSettings["ProdPath"];
                    
                }
                else
                {
                    container = database.GetContainer("CollectionsTesting");
                    PathValue = ConfigurationManager.AppSettings["TestPath"];
                }

               
            

            Int32 unixTimestampNow = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            dt1 = DateTime.UtcNow.AddMinutes(-int.Parse(ConfigurationManager.AppSettings["Interval"]));
                

            Int32 unixTimestampYesterday = (Int32)(dt1.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

            string query = "select * from c where (c._ts >= "+ unixTimestampYesterday +" AND " +" c._ts <="+ unixTimestampNow +")";
                        
            Console.WriteLine(query);

            // Query for an item
            FeedIterator<dynamic> feedIterator = container.GetItemQueryIterator<dynamic>(query);
            while (feedIterator.HasMoreResults)
            {
                FeedResponse<dynamic> response = await feedIterator.ReadNextAsync();

                foreach (var item in response)
                { 
                    try
                    {
                        string str1 = JsonConvert.SerializeObject(item);
                        //Console.WriteLine(str1);

                        var jo = JObject.Parse(str1);
                        var str = jo["document"]["UtteranceList"]["$values"].ToString();
                        var pid = jo["id"].ToString();
            //            Console.WriteLine(pid);
            //            Console.WriteLine(str);

                        using (System.IO.StreamWriter file = new System.IO.StreamWriter(PathValue + pid + ".txt", true))
                        {
                            file.WriteLine(str);
                        }

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error: {0}\n", e.Message);
                    }

                }
            }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
