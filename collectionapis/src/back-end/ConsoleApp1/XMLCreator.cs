﻿using System;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
using RestSharp;

namespace CollectionBatch
{
    class XMLCreator
    {
        static void Main(string[] args)
        {
            Console.WriteLine("XML Creator Batch Execution Start");
            WriteLog("XML Creator Batch Execution Start");
            Task<IRestResponse> res = GetRequest(ConfigurationManager.AppSettings["XMLCreatorAPI"]);
            WriteLog(res.Result.Content);
            WriteLog("XML Creator Batch Execution End");
            Console.WriteLine("XML Creator Batch Execution End");
        }
        public static void WriteLog(string strLog)
        {
            StreamWriter log;
            FileStream fileStream = null;
            DirectoryInfo logDirInfo = null;
            FileInfo logFileInfo;

            string logFilePath = ConfigurationManager.AppSettings["LogFilePath"];
            logFilePath = logFilePath + "Log-" + System.DateTime.Today.ToString("MM-dd-yyyy") + "." + "txt";
            logFileInfo = new FileInfo(logFilePath);
            logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
            if (!logDirInfo.Exists) logDirInfo.Create();
            if (!logFileInfo.Exists)
            {
                fileStream = logFileInfo.Create();
            }
            else
            {
                fileStream = new FileStream(logFilePath, FileMode.Append);
            }
            log = new StreamWriter(fileStream);
            log.WriteLine(strLog);
            log.Close();
        }
        public static async Task<IRestResponse> GetRequest(string baseUrl)
        {
            RestClient restCient = new RestClient(baseUrl);
            RestRequest request = new RestRequest(Method.GET);
            var responseData = await restCient.ExecuteTaskAsync(request);
            var response = responseData;
            return response;
        }
    }
}

