﻿using CollectionsAPIs.Utils;
using CollectionsAPIs.Utils.Exception;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionsAPIs.infra
{
    public class RestClientRequestFactory
    {
        /// <summary>Posts the request.</summary>
        /// <param name="baseUrl">The base URL.</param>
        /// <param name="requestBodyStr">The request body string.</param>
        /// <returns>System.String.</returns>
        public static async Task<IRestResponse> PostRequest(string baseUrl, string requestBodyStr, Dictionary<string, string> headerList,
                                                                string bearerAuthToken, string basicAuthUsername, string basicAuthPassword)
        {
            var client = new RestClient(baseUrl);
            var request = new RestRequest();
            request.Method = Method.POST;
            request.Parameters.Clear();
            request = AddRequestHeaders(request, headerList);
            string contentType = string.Empty;
            if (headerList != null)
            {
                contentType = headerList.Where(item => item.Key.Contains(APIConstants.CONTENT_TYPE)).Select(item => item.Value).SingleOrDefault();
            }
            if (!string.IsNullOrEmpty(contentType))
            {
                request.AddParameter(contentType, requestBodyStr, ParameterType.RequestBody);
            }
            request = AddAuthorizationHeader(request, bearerAuthToken, basicAuthUsername, basicAuthPassword);
            IRestResponse response = await client.ExecuteTaskAsync(request);
            if (!response.IsSuccessful)
            {
                throw new CollectionException(CollectionExceptionsConstants.EX_SOURCEAPIERROR);
            }
            else
            {
                return response;
            }
        }

        public static async Task<IRestResponse> GetRequest(string baseUrl, Dictionary<string, string> headerList,
                            string bearerAuthToken, string basicAuthUsername, string basicAuthPassword)
        {
            var client = new RestClient(baseUrl);
            var request = new RestRequest();
            request.Method = Method.GET;
            request.Parameters.Clear();
            request = AddRequestHeaders(request, headerList);
            request = AddAuthorizationHeader(request, bearerAuthToken, basicAuthUsername, basicAuthPassword);
            var response = await client.ExecuteTaskAsync(request);
            if (!response.IsSuccessful)
            {
                throw new CollectionException(CollectionExceptionsConstants.EX_SOURCEAPIERROR);
            }
            else
            {
                return response;
            }
        }

        private static RestRequest AddRequestHeaders(RestRequest restRequest, Dictionary<string, string> headerList)
        {
            if (headerList != null)
            {
                foreach (KeyValuePair<string, string> dic in headerList)
                {
                    restRequest.AddHeader(dic.Key, dic.Value);
                }
            }
            return restRequest;
        }

        private static RestRequest AddAuthorizationHeader(RestRequest restRequest, string bearerAuthToken, string basicAuthUsername, string basicAuthPassword)
        {
            if (string.IsNullOrEmpty(basicAuthUsername) && string.IsNullOrEmpty(basicAuthPassword))
            {
                restRequest.AddHeader(APIConstants.AUTHORIZATION, APIConstants.BASIC + EncodingForBase64.EncodeBase64(basicAuthUsername + ":" + basicAuthPassword));
            }
            if (string.IsNullOrEmpty(bearerAuthToken))
            {
                restRequest.AddParameter(APIConstants.AUTHORIZATION, APIConstants.BEARER + bearerAuthToken, ParameterType.HttpHeader);
            }
            return restRequest;
        }
    }
}
