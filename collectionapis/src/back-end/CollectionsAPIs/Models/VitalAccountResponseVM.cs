﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CollectionsAPIs.Models
{
    [XmlRoot(ElementName = "AccountInfoV2", Namespace = "http://microsoft.com/webservices/")]
    public class VitalAccountResponseVM
    {
        [XmlElement(ElementName = "AccountNo", Namespace = "http://microsoft.com/webservices/")]
        public string AccountNo { get; set; }
        [XmlElement(ElementName = "AccountName", Namespace = "http://microsoft.com/webservices/")]
        public string AccountName { get; set; }
        [XmlElement(ElementName = "Address1", Namespace = "http://microsoft.com/webservices/")]
        public string Address1 { get; set; }
        [XmlElement(ElementName = "Address2", Namespace = "http://microsoft.com/webservices/")]
        public string Address2 { get; set; }
        [XmlElement(ElementName = "City", Namespace = "http://microsoft.com/webservices/")]
        public string City { get; set; }
        [XmlElement(ElementName = "State", Namespace = "http://microsoft.com/webservices/")]
        public string State { get; set; }
        [XmlElement(ElementName = "Zip", Namespace = "http://microsoft.com/webservices/")]
        public string Zip { get; set; }
        [XmlElement(ElementName = "Phone", Namespace = "http://microsoft.com/webservices/")]
        public string Phone { get; set; }
        [XmlElement(ElementName = "Status", Namespace = "http://microsoft.com/webservices/")]
        public string Status { get; set; }
        [XmlElement(ElementName = "Balance", Namespace = "http://microsoft.com/webservices/")]
        public string Balance { get; set; }
        [XmlElement(ElementName = "Score", Namespace = "http://microsoft.com/webservices/")]
        public string Score { get; set; }
        [XmlElement(ElementName = "SifPct", Namespace = "http://microsoft.com/webservices/")]
        public string SifPct { get; set; }
        [XmlElement(ElementName = "Web_SIF_Only", Namespace = "http://microsoft.com/webservices/")]
        public string Web_SIF_Only { get; set; }
        [XmlElement(ElementName = "CliName", Namespace = "http://microsoft.com/webservices/")]
        public string CliName { get; set; }
        [XmlElement(ElementName = "OrigCreditor", Namespace = "http://microsoft.com/webservices/")]
        public string OrigCreditor { get; set; }
        [XmlElement(ElementName = "RequestStatus", Namespace = "http://microsoft.com/webservices/")]
        public string RequestStatus { get; set; }
        [XmlElement(ElementName = "Description", Namespace = "http://microsoft.com/webservices/")]
        public string Description { get; set; }
        [XmlElement(ElementName = "Principal", Namespace = "http://microsoft.com/webservices/")]
        public string Principal { get; set; }
        [XmlElement(ElementName = "Interest", Namespace = "http://microsoft.com/webservices/")]
        public string Interest { get; set; }
        [XmlElement(ElementName = "MiscFee", Namespace = "http://microsoft.com/webservices/")]
        public string MiscFee { get; set; }
        [XmlElement(ElementName = "DLP", Namespace = "http://microsoft.com/webservices/")]
        public string DLP { get; set; }
        [XmlElement(ElementName = "Orig_Acct_Num", Namespace = "http://microsoft.com/webservices/")]
        public string Orig_Acct_Num { get; set; }
        [XmlElement(ElementName = "ThreePSO_SINGLE_AMOUNT", Namespace = "http://microsoft.com/webservices/")]
        public string ThreePSO_SINGLE_AMOUNT { get; set; }
        [XmlElement(ElementName = "ThreePSO_TOTAL_AMOUNT", Namespace = "http://microsoft.com/webservices/")]
        public string ThreePSO_TOTAL_AMOUNT { get; set; }
        [XmlElement(ElementName = "ThreePSO_LETTER_SENT", Namespace = "http://microsoft.com/webservices/")]
        public string ThreePSO_LETTER_SENT { get; set; }
        [XmlElement(ElementName = "Processing_Fee", Namespace = "http://microsoft.com/webservices/")]
        public string Processing_Fee { get; set; }
        [XmlElement(ElementName = "Mandatory_SIF_Amount", Namespace = "http://microsoft.com/webservices/")]
        public string Mandatory_SIF_Amount { get; set; }
        [XmlElement(ElementName = "SIF_12Pay_11", Namespace = "http://microsoft.com/webservices/")]
        public string SIF_12Pay_11 { get; set; }
        [XmlElement(ElementName = "SIF_12Pay_Last", Namespace = "http://microsoft.com/webservices/")]
        public string SIF_12Pay_Last { get; set; }
        [XmlElement(ElementName = "SIF_12Pay_Total", Namespace = "http://microsoft.com/webservices/")]
        public string SIF_12Pay_Total { get; set; }
        [XmlElement(ElementName = "Credit_Card_OK", Namespace = "http://microsoft.com/webservices/")]
        public string Credit_Card_OK { get; set; }
        [XmlElement(ElementName = "Debit_Card_OK", Namespace = "http://microsoft.com/webservices/")]
        public string Debit_Card_OK { get; set; }
        [XmlElement(ElementName = "Money_On_File", Namespace = "http://microsoft.com/webservices/")]
        public string Money_On_File { get; set; }
        [XmlElement(ElementName = "SSN4", Namespace = "http://microsoft.com/webservices/")]
        public string SSN4 { get; set; }
        [XmlElement(ElementName = "DOB", Namespace = "http://microsoft.com/webservices/")]
        public string DOB { get; set; }
        [XmlElement(ElementName = "AltPhone1", Namespace = "http://microsoft.com/webservices/")]
        public string AltPhone1 { get; set; }
        [XmlElement(ElementName = "AltPhone2", Namespace = "http://microsoft.com/webservices/")]
        public string AltPhone2 { get; set; }
        [XmlElement(ElementName = "AltPhone3", Namespace = "http://microsoft.com/webservices/")]
        public string AltPhone3 { get; set; }
        [XmlElement(ElementName = "AltPhone4", Namespace = "http://microsoft.com/webservices/")]
        public string AltPhone4 { get; set; }
        [XmlElement(ElementName = "SixPart_Pay_5", Namespace = "http://microsoft.com/webservices/")]
        public string SixPart_Pay_5 { get; set; }
        [XmlElement(ElementName = "SixPart_Pay_Last", Namespace = "http://microsoft.com/webservices/")]
        public string SixPart_Pay_Last { get; set; }
        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        [JsonIgnore]
        public string Xsi { get; set; }
        [XmlAttribute(AttributeName = "xsd", Namespace = "http://www.w3.org/2000/xmlns/")]
        [JsonIgnore]
        public string Xsd { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        [JsonIgnore]
        public string Xmlns { get; set; }
        [XmlElement(ElementName = "CliRef", Namespace = "http://microsoft.com/webservices/")]
        public string CliRef { get; set; }


    }
}
