﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionsAPIs.Models
{
    public class CustomerVM
    {
        [JsonProperty("StreetAddress")]
        public string StreetAddress { get; set; }

        [JsonProperty("CityState")]
        public string CityState { get; set; }

        [JsonProperty("ZipCode")]
        public string ZipCode { get; set; }

        [JsonProperty("PrimaryPhone")]
        public string PrimaryPhone { get; set; }

        [JsonProperty("AlternatePhone1")]
        //[JsonConverter(typeof(ParseStringConverter))]
        public string AlternatePhone1 { get; set; }

        [JsonProperty("AlternatePhone2")]
        //[JsonConverter(typeof(ParseStringConverter))]
        public string AlternatePhone2 { get; set; }

        [JsonProperty("AlternatePhone3")]
        public string AlternatePhone3 { get; set; }

        [JsonProperty("AlternatePhone4")]
        //[JsonConverter(typeof(ParseStringConverter))]
        public string AlternatePhone4 { get; set; }

        [JsonProperty("EmailAddress")]
        public string EmailAddress { get; set; }
    }
}
