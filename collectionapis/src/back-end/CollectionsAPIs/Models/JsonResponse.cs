﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionsAPIs.Models
{
    public class JsonResponse
    {
        public JsonResponse()
        {
            statusCode = 0;
        }
        public bool status { get; set; }
        public int statusCode { get; set; }
        public string statusMessage { get; set; }
        public Object data { get; set; }
    }
}
