﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionsAPIs.Models
{
    public class AccountResponseVM
    {
        [JsonProperty("AccountNo")]
        public string AccountNo { get; set; }

        [JsonProperty("AccountName")]
        public string AccountName { get; set; }

        [JsonProperty("Address1")]
        public string Address1 { get; set; }

        [JsonProperty("Address2")]
        public string Address2 { get; set; }

        [JsonProperty("City")]
        public string City { get; set; }

        [JsonProperty("State")]
        public string State { get; set; }

        [JsonProperty("Zip")]
        public string Zip { get; set; }

        [JsonProperty("Phone")]
        public string Phone { get; set; }

        [JsonProperty("Status")]
        public string Status { get; set; }

        [JsonProperty("Balance")]
        public double Balance { get; set; }

        [JsonProperty("Score")]
        //[JsonConverter(typeof(StringToLongConverter))]
        public string Score { get; set; }

        [JsonProperty("SifPct")]
        //[JsonConverter(typeof(StringToLongConverter))]
        public string SifPct { get; set; }

        [JsonProperty("Web_SIF_Only")]
        public string WebSifOnly { get; set; }

        [JsonProperty("CliName")]
        public string CliName { get; set; }

        [JsonProperty("OrigCreditor")]
        public string OrigCreditor { get; set; }

        [JsonProperty("RequestStatus")]
        public string RequestStatus { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("Principal")]
        public double Principal { get; set; }

        [JsonProperty("Interest")]
        public long Interest { get; set; }

        [JsonProperty("MiscFee")]
        public long MiscFee { get; set; }

        [JsonProperty("DLP")]
        public string Dlp { get; set; }

        [JsonProperty("Orig_Acct_Num")]
        public string OrigAcctNum { get; set; }

        [JsonProperty("ThreePSO_SINGLE_AMOUNT")]
        public double ThreePsoSingleAmount { get; set; }

        [JsonProperty("ThreePSO_TOTAL_AMOUNT")]
        public double ThreePsoTotalAmount { get; set; }

        [JsonProperty("ThreePSO_LETTER_SENT")]
        public string ThreePsoLetterSent { get; set; }

        [JsonProperty("Processing_Fee")]
        public string ProcessingFee { get; set; }

        [JsonProperty("Mandatory_SIF_Amount")]
        public string MandatorySifAmount { get; set; }

        [JsonProperty("SIF_12Pay_11")]
        public string Sif12Pay11 { get; set; }

        [JsonProperty("SIF_12Pay_Last")]
        public string Sif12PayLast { get; set; }

        [JsonProperty("SIF_12Pay_Total")]
        public string Sif12PayTotal { get; set; }

        [JsonProperty("Credit_Card_OK")]
        public string CreditCardOk { get; set; }

        [JsonProperty("Debit_Card_OK")]
        public string DebitCardOk { get; set; }

        [JsonProperty("Money_On_File")]
        public string MoneyOnFile { get; set; }

        [JsonProperty("SSN4")]
        public string Ssn4 { get; set; }

        [JsonProperty("DOB")]
        public string Dob { get; set; }

        [JsonProperty("AltPhone1")]
        public string AltPhone1 { get; set; }

        [JsonProperty("AltPhone2")]
        public string AltPhone2 { get; set; }

        [JsonProperty("AltPhone3")]
        public string AltPhone3 { get; set; }

        [JsonProperty("AltPhone4")]
        public string AltPhone4 { get; set; }

        [JsonProperty("SixPart_Pay_5")]
        public double SixPartPay5 { get; set; }

        [JsonProperty("SixPart_Pay_Last")]
        public double SixPartPayLast { get; set; }

        [JsonProperty("CliRef")]
        public string CliRef { get; set; }


        public bool SsnValidationFlag
        {
            get
            {
                return string.IsNullOrEmpty(Ssn4) ? false : true;
            }
        }

        public bool DobValidationFlag
        {
            get
            {
                return string.IsNullOrEmpty(Dob) ? false : true;
            }
        }

        public bool AltPhoneValidationFlag
        {
            get
            {
                return !(string.IsNullOrEmpty(AltPhone1) & string.IsNullOrEmpty(AltPhone2) 
                    & string.IsNullOrEmpty(AltPhone3) & string.IsNullOrEmpty(AltPhone4));
            }
        }

        public List<string> AltPhoneList { get; set; }
    }

    internal class StringToLongConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(long) || t == typeof(long?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            long l;
            if (Int64.TryParse(value, out l))
            {
                return l;
            }
            throw new Exception("Cannot unmarshal type long");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (long)untypedValue;
            serializer.Serialize(writer, value.ToString());
            return;
        }

        public static readonly StringToLongConverter Singleton = new StringToLongConverter();
    }
}
