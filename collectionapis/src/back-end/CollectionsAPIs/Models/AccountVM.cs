﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionsAPIs.Models
{
    public class AccountVM
    {
        [JsonProperty("DebtorNum")]
        public string DebtorNum { get; set; }

        [JsonProperty("DebtorName")]
        public string DebtorName { get; set; }

        [JsonProperty("DebtorAddr")]
        public string DebtorAddr { get; set; }

        [JsonProperty("DebtorCity")]
        public string DebtorCity { get; set; }

        [JsonProperty("DebtorState")]
        public string DebtorState { get; set; }

        [JsonProperty("DebtorZIP")]
        public string DebtorZip { get; set; }

        [JsonProperty("HomeNum")]
        public string HomeNum { get; set; }

        [JsonProperty("OtherNum")]
        public string OtherNum { get; set; }

        [JsonProperty("WorkNum")]
        public string WorkNum { get; set; }

        [JsonProperty("Cell1Num")]
        public string Cell1Num { get; set; }

        [JsonProperty("Cell2Num")]
        public string Cell2Num { get; set; }

        [JsonProperty("ClientOffice")]
        public string ClientOffice { get; set; }

        [JsonProperty("ClientNum")]
        public string ClientNum { get; set; }

        [JsonProperty("ClientRef1")]
        public string ClientRef1 { get; set; }

        [JsonProperty("CollOffice")]
        public string CollOffice { get; set; }

        [JsonProperty("CollNum")]
        public string CollNum { get; set; }

        [JsonProperty("PlaceBal")]
        public string PlaceBal { get; set; }

        [JsonProperty("CurrBal")]
        public string CurrBal { get; set; }

        [JsonProperty("TotPayAmt")]
        public string TotPayAmt { get; set; }

        [JsonProperty("LastDatePU")]
        public string LastDatePu { get; set; }

        [JsonProperty("WorkDate")]
        public string WorkDate { get; set; }

        [JsonProperty("AcctStatus")]
        public string AcctStatus { get; set; }

        [JsonProperty("PlaceDate")]
        public string PlaceDate { get; set; }

        [JsonProperty("LetterCode")]
        public string LetterCode { get; set; }

        [JsonProperty("LetterDate")]
        public string LetterDate { get; set; }

        [JsonProperty("CredScore")]
        public string CredScore { get; set; }

        [JsonProperty("Decile")]
        public string Decile { get; set; }

        [JsonProperty("LastDial")]
        public string LastDial { get; set; }

        [JsonProperty("ChargeOff")]
        public string ChargeOff { get; set; }

        [JsonProperty("DOB")]
        public string Dob { get; set; }

        [JsonProperty("OrigCred")]
        public string OrigCred { get; set; }

        [JsonProperty("LastDatePC")]
        public string LastDatePc { get; set; }

        [JsonProperty("DaysPastDue")]
        public string DaysPastDue { get; set; }

        [JsonProperty("LastDateRPC")]
        public string LastDateRpc { get; set; }

        [JsonProperty("LastDateMsg")]
        public string LastDateMsg { get; set; }

        [JsonProperty("ClientRef2")]
        public string ClientRef2 { get; set; }
    }
}
