﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionsAPIs.Models
{
    public class ContentServer
    {
        public string DataPath { get; set; }
        public string XMLPath { get; set; }
    }
}
