﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionsAPIs.Models
{
    public class VitalConfig
    {
        public string AccountUrl { get; set; }
        public string PaymentUrl { get; set; }
        public string AccountUrlVersion { get; set; }
        public string AccountUrlVersion1 { get; set; }
        public string FTPUrl { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
