﻿using CollectionsAPIs.Models;
using CollectionsAPIs.Services;
using CollectionsAPIs.Utils;
using GemBox.Spreadsheet;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsAPIs.Controllers
{
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        ILogger _logger;
        AccountSvc _accountSvc;

        public AccountController(ILogger logger, AccountSvc accountSvc)
        {
            _logger = logger;
            _accountSvc = accountSvc;
        }

     /*   [HttpGet("{pid}")]
        public JsonResponse GetAccountDetails(string pid)
        {
            AccountVM accountVM = _accountSvc.GetAccountById(pid);
            if (accountVM != null)
            {
                return new JsonResponse() { status = true, data = accountVM };
            }
            else
            {
                _logger.LogInfo("No Account Found with this PID.");
                return new JsonResponse() { status = false, statusMessage = "Account Not found" };
            }
        }
        */
        [HttpGet("vital/{pid}")]
        public JsonResponse GetVitalAccountDetails(string pid)
        {

            AccountResponseVM accountResponse = _accountSvc.FetchAccountDetailsFromVital(pid);
            if (!accountResponse.RequestStatus.Equals(VitalApiConstants.ERROR_RESPONSE_STATUS, StringComparison.OrdinalIgnoreCase))
            {
                _logger.LogInfo("Successfully Fetch Account Details From Vital.");
                return new JsonResponse() { status = true, data = accountResponse, statusMessage = accountResponse.Description };
            }
            else
            {
                _logger.LogInfo("Invalid Account Number");
                return new JsonResponse() { status = false, statusMessage = accountResponse != null ? accountResponse.Description : "Invalid Account Number" };
            }
        }

        [HttpGet("vital/clientrf/{clientref}")]
        public JsonResponse GetVitalAccountDetailsByclientref(String clientref,[FromQuery] string SSN4, [FromQuery]string DOBMMYYYY,[FromQuery] string ZIP5)
        {
            string  dob="", zip="", ssn="";
            /*if (!String.IsNullOrEmpty(DOBMMYYYY))
                dob = HttpContext.Request.Query["SSN4"];
            if (!String.IsNullOrEmpty(HttpContext.Request.Query["DOBMMYYYY"]))
                zip = HttpContext.Request.Query["DOBMMYYYY"];
            if (!String.IsNullOrEmpty(HttpContext.Request.Query["ZIP5"]))
                ssn = HttpContext.Request.Query["ZIP5"];*/
            AccountResponseVM accountResponse = _accountSvc.FetchAccountDetailsFromVitalClientid(clientref, DOBMMYYYY, ZIP5, SSN4);
            if (!accountResponse.RequestStatus.Equals(VitalApiConstants.ERROR_RESPONSE_STATUS, StringComparison.OrdinalIgnoreCase))
            {
                _logger.LogInfo("Successfully Fetch Account Details From Vital.");
                return new JsonResponse() { status = true, data = accountResponse, statusMessage = accountResponse.Description };
            }
            else
            {
                _logger.LogInfo("Invalid Account Number");
                return new JsonResponse() { status = false, statusMessage = accountResponse != null ? accountResponse.Description : "Invalid Account Number" };
            }
        }

       
    }
}
