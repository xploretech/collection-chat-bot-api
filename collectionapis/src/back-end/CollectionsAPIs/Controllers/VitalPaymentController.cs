﻿using CollectionsAPIs.Models;
using CollectionsAPIs.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionsAPIs.Controllers
{
    [Route("api/[controller]")]
    public class VitalPaymentController : Controller
    {
        ILogger _logger;
        VitalConfig _vitalConfig;

        public VitalPaymentController(ILogger logger, IOptions<VitalConfig> vitalConfig)
        {
            _logger = logger;
            _vitalConfig = vitalConfig.Value;
        }

        [HttpGet("{pid}/{zip}")]
        public void GetVitalAccountDetails(string pid, string zip)
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl(_vitalConfig.PaymentUrl);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtPaymentID")).SendKeys(pid);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_txtZipCode")).SendKeys(zip);
            driver.FindElement(By.Id("ctl00_ContentPlaceHolder1_btnSubmit")).Click();
        }
    }
}
