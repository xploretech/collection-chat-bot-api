﻿using CollectionsAPIs.Models;
using CollectionsAPIs.Services;
using DocumentFormat.OpenXml.Bibliography;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace CollectionsAPIs.Controllers
{
    [Route("api/[controller]")]
    public class CustomerController : Controller
    {
        CustomerSvc _customerSvc;
        public CustomerController(CustomerSvc customerSvc)
        {
            _customerSvc = customerSvc;
        }
        [HttpPost("update/{debtorNumber}")]
        public JsonResponse PostCustomerDetails([FromBody] CustomerVM details, string debtorNumber)
        {
            string _debtorNumber = debtorNumber;
            string ret = _customerSvc.SaveUserDetails(details, _debtorNumber);
            if (ret == null)
            {
                return new JsonResponse() { status = false, statusMessage = "Error on Update" };
            }
            else
            {
                return new JsonResponse() { status = true, statusMessage = "Customer Details Updated" };
            }
        }
        [HttpGet("updateddata")]
        public JsonResponse DayEndUpdatedData()
        {
            var ret = _customerSvc.GetDataWithDate();
            if(ret == true)
            {
                return new JsonResponse() { status = true, statusMessage = "XML File Created" };
            }
            else
            {
                return new JsonResponse() { status = true, statusMessage = "XML File Not Created" };
            }
        }
    }
}
