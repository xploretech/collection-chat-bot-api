﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TectesInfra.GenericRepo.Entities;
using TectesInfra.GenericRepo.Interfaces;
using TectesInfra.GenericRepo.UoW;

namespace CollectionsAPIs.Services
{
    public class BaseSvc
    {
        protected IUoW _uow;
        protected BaseSvc(IUoW uow)
        {
            _uow = uow;
        }
        protected IGenericRepo<Domain, long> TypedGenericRepo<Domain>() where Domain : BaseTrackableEntity<long>
        {
            return _uow.GenericRepo<Domain, long>();
        }
    }
}
