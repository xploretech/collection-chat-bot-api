﻿using CollectionsAPIs.infra;
using CollectionsAPIs.Models;
using CollectionsAPIs.Utils;
using GemBox.Spreadsheet;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;

namespace CollectionsAPIs.Services
{
    public class AccountSvc
    {
        ILogger _logger;
        VitalConfig _vitalConfig;
        ContentServer _contentServer;

        public AccountSvc(ILogger logger, IOptions<VitalConfig> vitalConfig, IOptions<ContentServer> contentServer)
        {
            _logger = logger;
            _contentServer = contentServer.Value;
            _vitalConfig = vitalConfig.Value;
        }

        public AccountVM GetAccountById(string pid)
        {
            _logger.LogInfo("Start Account Searching");
            var workbook = LoadAllAccountDetailsFromExcel("FREE-LIMITED-KEY", _contentServer.DataPath);
            List<AccountVM> accountVMList = PopulateAccountListFromExcelSheet(workbook.Worksheets.ActiveWorksheet);
            AccountVM accountVM = accountVMList.Where(e => Equals(e.DebtorNum, pid)).FirstOrDefault();
            _logger.LogInfo("End Account Searching");
            return accountVM;
        }

        public List<AccountVM> PopulateAccountListFromExcelSheet(ExcelWorksheet worksheet)
        {
            List<AccountVM> accountVMList = new List<AccountVM>();
            for (int row = 1; row < worksheet.Rows.Count(); row++)
            {
                AccountVM accountVM = new AccountVM()
                {
                    DebtorNum = worksheet.Cells[row, 0].Value == null ? string.Empty : worksheet.Cells[row, 0].Value.ToString(),
                    DebtorName = worksheet.Cells[row, 1].Value == null ? string.Empty : worksheet.Cells[row, 1].Value.ToString(),
                    DebtorAddr = worksheet.Cells[row, 2].Value == null ? string.Empty : worksheet.Cells[row, 2].Value.ToString(),
                    DebtorCity = worksheet.Cells[row, 3].Value == null ? string.Empty : worksheet.Cells[row, 3].Value.ToString(),
                    DebtorState = worksheet.Cells[row, 4].Value == null ? string.Empty : worksheet.Cells[row, 4].Value.ToString(),
                    DebtorZip = worksheet.Cells[row, 5].Value == null ? string.Empty : worksheet.Cells[row, 5].Value.ToString(),
                    HomeNum = worksheet.Cells[row, 6].Value == null ? string.Empty : worksheet.Cells[row, 6].Value.ToString(),
                    OtherNum = worksheet.Cells[row, 7].Value == null ? string.Empty : worksheet.Cells[row, 7].Value.ToString(),
                    WorkNum = worksheet.Cells[row, 8].Value == null ? string.Empty : worksheet.Cells[row, 8].Value.ToString(),
                    Cell1Num = worksheet.Cells[row, 9].Value == null ? string.Empty : worksheet.Cells[row, 9].Value.ToString(),
                    Cell2Num = worksheet.Cells[row, 10].Value == null ? string.Empty : worksheet.Cells[row, 10].Value.ToString(),
                    ClientOffice = worksheet.Cells[row, 11].Value == null ? string.Empty : worksheet.Cells[row, 11].Value.ToString(),
                    ClientNum = worksheet.Cells[row, 12].Value == null ? string.Empty : worksheet.Cells[row, 12].Value.ToString(),
                    ClientRef1 = worksheet.Cells[row, 13].Value == null ? string.Empty : worksheet.Cells[row, 13].Value.ToString(),
                    CollOffice = worksheet.Cells[row, 14].Value == null ? string.Empty : worksheet.Cells[row, 14].Value.ToString(),
                    CollNum = worksheet.Cells[row, 15].Value == null ? string.Empty : worksheet.Cells[row, 15].Value.ToString(),
                    PlaceBal = worksheet.Cells[row, 16].Value == null ? string.Empty : worksheet.Cells[row, 16].Value.ToString(),
                    CurrBal = worksheet.Cells[row, 17].Value == null ? string.Empty : worksheet.Cells[row, 17].Value.ToString(),
                    TotPayAmt = worksheet.Cells[row, 18].Value == null ? string.Empty : worksheet.Cells[row, 18].Value.ToString(),
                    LastDatePu = worksheet.Cells[row, 19].Value == null ? string.Empty : worksheet.Cells[row, 19].Value.ToString(),
                    WorkDate = worksheet.Cells[row, 20].Value == null ? string.Empty : worksheet.Cells[row, 20].Value.ToString(),
                    AcctStatus = worksheet.Cells[row, 21].Value == null ? string.Empty : worksheet.Cells[row, 21].Value.ToString(),
                    PlaceDate = worksheet.Cells[row, 22].Value == null ? string.Empty : worksheet.Cells[row, 22].Value.ToString(),
                    LetterCode = worksheet.Cells[row, 23].Value == null ? string.Empty : worksheet.Cells[row, 23].Value.ToString(),
                    LetterDate = worksheet.Cells[row, 24].Value == null ? string.Empty : worksheet.Cells[row, 24].Value.ToString(),
                    CredScore = worksheet.Cells[row, 25].Value == null ? string.Empty : worksheet.Cells[row, 25].Value.ToString(),
                    Decile = worksheet.Cells[row, 26].Value == null ? string.Empty : worksheet.Cells[row, 26].Value.ToString(),
                    LastDial = worksheet.Cells[row, 27].Value == null ? string.Empty : worksheet.Cells[row, 27].Value.ToString(),
                    ChargeOff = worksheet.Cells[row, 28].Value == null ? string.Empty : worksheet.Cells[row, 28].Value.ToString(),
                    Dob = worksheet.Cells[row, 29].Value == null ? string.Empty : worksheet.Cells[row, 29].Value.ToString(),
                    OrigCred = worksheet.Cells[row, 30].Value == null ? string.Empty : worksheet.Cells[row, 30].Value.ToString(),
                    LastDatePc = worksheet.Cells[row, 31].Value == null ? string.Empty : worksheet.Cells[row, 31].Value.ToString(),
                    DaysPastDue = worksheet.Cells[row, 32].Value == null ? string.Empty : worksheet.Cells[row, 32].Value.ToString(),
                    LastDateRpc = worksheet.Cells[row, 33].Value == null ? string.Empty : worksheet.Cells[row, 33].Value.ToString(),
                    LastDateMsg = worksheet.Cells[row, 34].Value == null ? string.Empty : worksheet.Cells[row, 34].Value.ToString(),
                    ClientRef2 = worksheet.Cells[row, 35].Value == null ? string.Empty : worksheet.Cells[row, 35].Value.ToString(),
                };
                accountVMList.Add(accountVM);
            }
            return accountVMList;
        }
        /* Load Excel from the particular file path*/
        public ExcelFile LoadAllAccountDetailsFromExcel(string licenseKey, string filePath)
        {
            SpreadsheetInfo.SetLicense(licenseKey);
            _logger.LogInfo("Start Loading data from Excel File");
            // Load Excel file.
            var workbook = ExcelFile.Load(filePath);
            _logger.LogInfo("Completed Loaded data from Excel File");
            return workbook;
        }

        public AccountResponseVM FetchAccountDetailsFromVital(string pid)
        {
            if (pid.Length <= 9)
                pid = CalculatePID(pid);

            _logger.LogInfo("Start Fetching Account Details From Vital for PID : " + pid + ".");
            var response = RestClientRequestFactory.GetRequest(_vitalConfig.AccountUrl + _vitalConfig.AccountUrlVersion + "?AccountNo=" + pid, null, null, null, null);
            _logger.LogInfo("End Fetching Account Details From Vital for PID : " + pid + ".");
            VitalAccountResponseVM accountInfo = XmlConverter.DeserializeObject<VitalAccountResponseVM>(response.Result.Content);
            
            return GetAccountResponseVM(accountInfo);
        }

        public AccountResponseVM FetchAccountDetailsFromVitalClientid(string clientid,string dob,string zip,string ssn)
        {
            
            _logger.LogInfo("Start Fetching Account Details From Vital for PID : " + clientid + ".");
            var response = RestClientRequestFactory.GetRequest(_vitalConfig.AccountUrl + _vitalConfig.AccountUrlVersion1 + "?CliRef=" + clientid + "&SSN4=" + ssn + "&DOBMMYYYY=" + dob + "&ZIP5=" + zip, null, null, null, null);
            _logger.LogInfo("End Fetching Account Details From Vital for PID : " + clientid + ".");
            VitalAccountResponseVM accountInfo = XmlConverter.DeserializeObject<VitalAccountResponseVM>(response.Result.Content);

            return GetAccountResponseVM(accountInfo);
        }




        public string CalculatePID(string no)
        {
            int number = 0,total=0;
            string result;
            foreach(char c in no)
            {
                bool isnumber = int.TryParse(c.ToString(), out number);
                if (isnumber)
                    total = total + number;
            }
            if (total < 10)
                result = "0" + total.ToString();
            else
                result = total.ToString();
            return no + result;
        }

        public AccountResponseVM GetAccountResponseVM(VitalAccountResponseVM vitalAccountResponse)
        {
            _logger.LogInfo("Start Model Conversion From Vital to Collection ");
            AccountResponseVM accountResponseVM = new AccountResponseVM()
            {
                AccountNo = vitalAccountResponse.AccountNo,
                AccountName = vitalAccountResponse.AccountName,
                Address1 = vitalAccountResponse.Address1,
                Address2 = vitalAccountResponse.Address2,
                City = vitalAccountResponse.City,
                State = vitalAccountResponse.State,
                Zip = vitalAccountResponse.Zip,
                Phone = vitalAccountResponse.Phone,
                Status = vitalAccountResponse.Status,
                Score= vitalAccountResponse.Score,
                SifPct = vitalAccountResponse.SifPct,
                WebSifOnly = vitalAccountResponse.Web_SIF_Only,
                CliName = vitalAccountResponse.CliName,
                OrigCreditor = vitalAccountResponse.OrigCreditor,
                RequestStatus = vitalAccountResponse.RequestStatus,
                Description = vitalAccountResponse.Description,
                Dlp = vitalAccountResponse.DLP,
                OrigAcctNum = vitalAccountResponse.Orig_Acct_Num,
                ThreePsoLetterSent = vitalAccountResponse.ThreePSO_LETTER_SENT,
                ProcessingFee = vitalAccountResponse.Processing_Fee,
                MandatorySifAmount = vitalAccountResponse.Mandatory_SIF_Amount,
                Sif12Pay11 = vitalAccountResponse.SIF_12Pay_11,
                Sif12PayLast = vitalAccountResponse.SIF_12Pay_Last,
                Sif12PayTotal = vitalAccountResponse.SIF_12Pay_Total,
                CreditCardOk = vitalAccountResponse.Credit_Card_OK,
                DebitCardOk = vitalAccountResponse.Debit_Card_OK,
                MoneyOnFile = vitalAccountResponse.Money_On_File,
                Ssn4 = vitalAccountResponse.SSN4,
                Dob = vitalAccountResponse.DOB,
                AltPhone1 = vitalAccountResponse.AltPhone1,
                AltPhone2 = vitalAccountResponse.AltPhone2,
                AltPhone3 = vitalAccountResponse.AltPhone3,
                AltPhone4 = vitalAccountResponse.AltPhone4,
                CliRef=vitalAccountResponse.CliRef
            };
            accountResponseVM.Balance = ConversionUtils.ConvertStringToDouble(vitalAccountResponse.Balance);
            accountResponseVM.Principal = ConversionUtils.ConvertStringToDouble(vitalAccountResponse.Principal);
            accountResponseVM.ThreePsoSingleAmount = ConversionUtils.ConvertStringToDouble(vitalAccountResponse.ThreePSO_SINGLE_AMOUNT);
            accountResponseVM.ThreePsoTotalAmount = ConversionUtils.ConvertStringToDouble(vitalAccountResponse.ThreePSO_TOTAL_AMOUNT);
            accountResponseVM.SixPartPay5 = ConversionUtils.ConvertStringToDouble(vitalAccountResponse.SixPart_Pay_5);
            accountResponseVM.SixPartPayLast = ConversionUtils.ConvertStringToDouble(vitalAccountResponse.SixPart_Pay_Last);
            accountResponseVM.AltPhoneList = new List<string>()
            {
                !string.IsNullOrEmpty(accountResponseVM.AltPhone1) ? accountResponseVM.AltPhone1 : null,
                !string.IsNullOrEmpty(accountResponseVM.AltPhone2) ? accountResponseVM.AltPhone2 : null,
                !string.IsNullOrEmpty(accountResponseVM.AltPhone3) ? accountResponseVM.AltPhone3 : null,
                !string.IsNullOrEmpty(accountResponseVM.AltPhone4) ? accountResponseVM.AltPhone4 : null
            };
            accountResponseVM.AltPhoneList.RemoveAll(e => e == null);
            _logger.LogInfo("End Model Conversion From Vital to Collection.");
            return accountResponseVM;
        }

    }
}

