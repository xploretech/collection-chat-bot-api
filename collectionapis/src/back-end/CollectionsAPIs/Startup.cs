﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Serilog;
using Serilog.Exceptions;
using CollectionsAPIs.Utils.Filter;
using CollectionsAPIs.Utils;
using CollectionsAPIs.Services;
using CollectionsAPIs.Models;
using System.Data.Entity;
using CollectionDomain;
using TectesInfra.GenericRepo.UoW;

namespace CollectionsAPIs
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            Log.Logger = new LoggerConfiguration()
                                .ReadFrom.Configuration(configuration)
                                .Enrich.FromLogContext()
                                .Enrich.WithExceptionDetails()
                                .CreateLogger();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .SetIsOriginAllowedToAllowWildcardSubdomains()

                    .AllowCredentials());
            });
            services.AddOptions();

            services.Configure<ContentServer>(Configuration.GetSection("ContentServer"));
            services.Configure<VitalConfig>(Configuration.GetSection("VitalConfig"));

            services.AddScoped<AccountSvc>();
            string abc = Configuration.GetConnectionString("DefaultConnection");
            services.AddScoped<DbContext>(_ => new CustomerContext(Configuration.GetConnectionString("DefaultConnection")));
            services.AddScoped(typeof(Utils.ILogger), typeof(SeriLogger));
            services.AddScoped<CustomerSvc>();
            services.AddScoped(typeof(IUoW), typeof(UoW));

            services.AddMvc(s => {
                s.Filters.Add(typeof(CollectionExceptionFilter));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseAuthentication();
            app.UseStaticFiles();
            app.UseCors("CorsPolicy");
            //start logging to the console
            var logger = loggerFactory.CreateLogger<ConsoleLogger>();
            logger.LogTrace("Executing Configure()");
            
            //app.UseResponseCompression();
            app.UseMvc();
        }
    }
}
