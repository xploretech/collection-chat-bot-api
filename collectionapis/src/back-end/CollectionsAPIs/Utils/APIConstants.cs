﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionsAPIs.Utils
{
    public class APIConstants
    {
        public const string CONTENT_TYPE = "Content-Type";
        public const string APPLICATION_JSON = "application/json";
        public const string APPLICATION_X_WWW_FORM_URLENCODED = "application/x-www-form-urlencoded";
        public const string ACCEPT = "Accept";
        public const string AUTHORIZATION = "Authorization";
        public const string BEARER = "Bearer ";
        public const string BASIC = "Basic ";
    }
}
