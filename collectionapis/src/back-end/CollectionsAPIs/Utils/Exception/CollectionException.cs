﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionsAPIs.Utils.Exception
{
    public class CollectionException  : System.Exception
    {
        public static Dictionary<int, String> EXCEPTIONDEF = new Dictionary<int, string>()
        {
            { CollectionExceptionsConstants.EX_SOURCEAPIEMPTYRESULTS , "No Result Found." },
            { CollectionExceptionsConstants.EX_SOURCEAPIERROR , "Error in account Search from provider" }
        };

        public int code { get; }

        public CollectionException(int code)
            : base(EXCEPTIONDEF[code])
        {
            this.code = code;
        }

        public CollectionException(int code, System.Exception innerException)
        : base(EXCEPTIONDEF[code], innerException)
        { }

        public CollectionException(System.Exception exception)
        : base(exception.Message)
        { }
    }
}
