﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionsAPIs.Utils.Exception
{
    public class CollectionExceptionsConstants
    {
        public static int EX_SOURCEAPIEMPTYRESULTS = 5101;
        public static int EX_SOURCEAPIERROR = 5100;
    }
}
