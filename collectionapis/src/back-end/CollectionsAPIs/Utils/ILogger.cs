﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionsAPIs.Utils
{
    public interface ILogger
    {
        void LogError(string message);
        void LogError<T>(string message) where T : new();
        void LogInfo(string message);
        void LogInfo<T>(string message) where T : new();
        void LogWarning(string message);
    }
}
