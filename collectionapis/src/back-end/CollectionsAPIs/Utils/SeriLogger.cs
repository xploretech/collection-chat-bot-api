﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionsAPIs.Utils
{
    public class SeriLogger : ILogger
    {
        public void LogError(string message)
        {
            Log.Logger.Error(message);
        }

        public void LogError<T>(string message) where T : new()
        {
            throw new NotImplementedException();
        }

        public void LogInfo(string message)
        {
            Log.Logger.Information(message);
        }

        public void LogInfo<T>(string message) where T : new()
        {
            throw new NotImplementedException();
        }

        public void LogWarning(string message)
        {
            Log.Logger.Warning(message);
        }
    }
}
