﻿using CollectionsAPIs.Models;
using CollectionsAPIs.Utils.Exception;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace CollectionsAPIs.Utils.Filter
{
    public class CollectionExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            HttpStatusCode status = HttpStatusCode.InternalServerError;
            JsonResponse response = new JsonResponse() { status = false };

            var exceptionType = context.Exception.GetType();
            if (exceptionType == typeof(UnauthorizedAccessException))
            {
                response.statusMessage = "Unauthorized Access";
                status = HttpStatusCode.Unauthorized;
            }
            else if (exceptionType == typeof(NotImplementedException))
            {
                response.statusMessage = "A server error occurred.";
                status = HttpStatusCode.NotImplemented;
            }
            else if (exceptionType == typeof(CollectionException))
            {
                CollectionException ex = (CollectionException)context.Exception;
                response.statusMessage = context.Exception.Message;
                response.statusCode = (short)ex.code;
                status = HttpStatusCode.OK;
            }
            else
            {
                response.statusMessage = context.Exception.Message;
                status = HttpStatusCode.BadRequest;
            }

            if (response.statusMessage == null)
                response.statusMessage = CollectionException.EXCEPTIONDEF[CollectionExceptionsConstants.EX_SOURCEAPIEMPTYRESULTS];

            Log.Logger.Error(context.Exception.ToString());
            if (context.Exception.InnerException != null)
                Log.Logger.Error(context.Exception.InnerException.ToString());

            context.ExceptionHandled = true;
            HttpResponse httpResponse = context.HttpContext.Response;
            httpResponse.StatusCode = (int)status;
            httpResponse.ContentType = "application/json";
            context.Result = new JsonResult(response);


        }
    }
}
