﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace CollectionsAPIs
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var hostUrl = "http://0.0.0.0:9003";
            var host = BuildWebHost(hostUrl);
            host.Run();
        }

        public static IWebHost BuildWebHost(string hostUrl) =>
           WebHost.CreateDefaultBuilder()
               .UseUrls(hostUrl)
               .UseIISIntegration()
               .UseStartup<Startup>()
               .Build();
    }
}
