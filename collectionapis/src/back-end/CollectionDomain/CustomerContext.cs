namespace CollectionDomain
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class CustomerContext : DbContext
    {
        public CustomerContext(String connString)
            : base(connString)
        {
        }

        public virtual DbSet<UpdateCustomerInfo> UpdateCustomerInfoes { get; set; }
        public virtual DbSet<UpdateCustomerURLTimer> UpdateCustomerURLTimer { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UpdateCustomerInfo>()
                .Property(e => e.street_address)
                .IsUnicode(false);

            modelBuilder.Entity<UpdateCustomerInfo>()
                .Property(e => e.city_state)
                .IsUnicode(false);

            modelBuilder.Entity<UpdateCustomerInfo>()
                .Property(e => e.zip_code)
                .IsUnicode(false);

            modelBuilder.Entity<UpdateCustomerInfo>()
                .Property(e => e.primary_phone)
                .IsUnicode(false);

            modelBuilder.Entity<UpdateCustomerInfo>()
                .Property(e => e.alternate_phone1)
                .IsUnicode(false);

            modelBuilder.Entity<UpdateCustomerInfo>()
                .Property(e => e.alternate_phone2)
                .IsUnicode(false);

            modelBuilder.Entity<UpdateCustomerInfo>()
                .Property(e => e.alternate_phone3)
                .IsUnicode(false);

            modelBuilder.Entity<UpdateCustomerInfo>()
                .Property(e => e.alternate_phone4)
                .IsUnicode(false);

            modelBuilder.Entity<UpdateCustomerInfo>()
                .Property(e => e.email_address)
                .IsUnicode(false);

            modelBuilder.Entity<UpdateCustomerInfo>()
               .Property(e => e.DebtorNumber)
               .IsUnicode(false);

            modelBuilder.Entity<UpdateCustomerURLTimer>()
               .Property(e => e.URLHashCode)
               .IsUnicode(false);

        }
    }
}
