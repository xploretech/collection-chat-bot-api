﻿namespace CollectionDomain
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using TectesInfra.GenericRepo.Entities;

    [Table("UpdateCustomerURLTimer")]
    public partial class UpdateCustomerURLTimer : BaseTrackableEntity<long>
    {
        public string URLHashCode { get; set; }
    }
}
