namespace CollectionDomain
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using TectesInfra.GenericRepo.Entities;

    [Table("UpdateCustomerInfo")]
    public partial class UpdateCustomerInfo : BaseTrackableEntity<long>
    {

        [StringLength(50)]
        public string street_address { get; set; }

        [StringLength(50)]
        public string city_state { get; set; }

        [StringLength(50)]
        public string zip_code { get; set; }

        [StringLength(50)]
        public string primary_phone { get; set; }

        [StringLength(50)]
        public string alternate_phone1 { get; set; }

        [StringLength(50)]
        public string alternate_phone2 { get; set; }

        [StringLength(50)]
        public string alternate_phone3 { get; set; }

        [StringLength(50)]
        public string alternate_phone4 { get; set; }

        [StringLength(50)]
        public string email_address { get; set; }
        [StringLength(50)]
        public string DebtorNumber { get; set; }
    }
}
